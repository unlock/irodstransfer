package nl.wur.ssb;

import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.FileNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.exception.OverwriteException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;

public class Upload {
    static final org.apache.log4j.Logger logger = Logger.getLogger(App.class);

    public static void uploadCollectionContent(CommandOptions commandOptions, Connection connection, File localFolder) throws JargonException {
        uploadCollectionContent(commandOptions, connection, localFolder, null);
    }

    public static void uploadCollectionContent(CommandOptions commandOptions, Connection connection, File localFolder, File targetFolder) throws JargonException {
        // Specify the irods target
        if (targetFolder == null) {
            if (new File(commandOptions.irodsDirectory).getName().equals(localFolder.getName())) {
                targetFolder = new File(commandOptions.irodsDirectory);
            } else{
                targetFolder = new File(commandOptions.irodsDirectory + "/" + localFolder.getName());
            }
        } else {
            targetFolder = new File(targetFolder + "/" + localFolder.getName());
        }

        if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFolder.getName()).exists()) {
            if (!connection.fileFactory.instanceIRODSFile(targetFolder.getAbsolutePath()).exists()) {
                connection.fileFactory.instanceIRODSFile(targetFolder.getAbsolutePath()).mkdirs();
            }
        }

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        for (File localFile : localFolder.listFiles()) {
            try {
                if (localFile.isDirectory()) {
                    logger.debug("Directory detected");
                    uploadCollectionContent(commandOptions, connection, localFile, targetFolder);
                } else {
                    IRODSFile destFile = connection.fileFactory.instanceIRODSFile(targetFolder + "/" + localFile.getName());
                    StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
                    TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
                    TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                    transferOptions.setIntraFileStatusCallbacks(true);

                    if (commandOptions.force) {
                        transferOptions.setForceOption(TransferOptions.ForceOption.USE_FORCE);
                    } else if (destFile.exists()) {
                      logger.warn("Skipping as destination file already exists and no force option is used " + destFile.getAbsolutePath());
                      continue;
                    } else {
                        transferOptions.setForceOption(TransferOptions.ForceOption.NO_FORCE);
                    }

                    transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
                    transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
                    defaultTransferControlBlock.setTransferOptions(transferOptions);
                    dataTransferOperationsAO.putOperation(localFile, destFile, statusCallbackListener, defaultTransferControlBlock);
                    logger.info("Transfer completed of " + localFile.getName());
                }
            } catch (JargonException e) {
                logger.error(e.getMessage());
            }
        }
    }

    public static void uploadFile(CommandOptions commandOptions, Connection connection, File localFile) throws JargonException {
        uploadFile(commandOptions, connection, localFile, false);
    }

    public static void uploadFile(CommandOptions commandOptions, Connection connection, File localFile, boolean rename) throws JargonException {

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // If path does not exist but parent does this means a file rename


        // If this is an existing directory, the filename needs to be appended to it
//        if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "").isDirectory()) {
//            if (!connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory.getParentFile() + "").isDirectory()) {
//                throw new ResourceDoesNotExistException("The destination is not an existing folder: " + commandOptions.irodsDirectory);
//            }
//        }

        if (connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFile.getName()).exists()) {
            if (connection.fileFactory.instanceIRODSFile(commandOptions.irodsDirectory + "/" + localFile.getName()).isDirectory()) {
                throw new FileNotFoundException("Folder already exists with the name of the file you are trying to upload");
            }
            logger.warn("File already exists");
            if (!commandOptions.force) {
                return;
            }
        }
        logger.info("Uploading " + localFile.getName() + " to " + commandOptions.irodsDirectory);

        // Default is to keep the name
        String target = commandOptions.irodsDirectory + "/" + localFile.getName();
        // Unless we need to rename it
        if (rename) {
            target = commandOptions.irodsDirectory;
        }
        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(target);
        dataTransferOperationsAO.putOperation(localFile, destFile, null, null);
    }
}
