package nl.wur.ssb;

import org.apache.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;

public class Download {
    static final org.apache.log4j.Logger logger = Logger.getLogger(Download.class);

    /**
     * Downloads the content of a collection (folder)
     *
     * @param commandOptions general arguments
     * @param collection     the specific folder to be downloaded
     * @throws JargonException when collection does not exists
     */
    public static void downloadCollectionContent(CommandOptions commandOptions, Connection connection, File collection) throws JargonQueryException, JargonException, GenQueryBuilderException {
        // Get all files from this directory including subdirectories

        // Remote folder exists as checked in previous function...
        IRODSFile irodsCollection = connection.fileFactory.instanceIRODSFile(String.valueOf(collection));
        if (!irodsCollection.exists()) {
            throw new JargonException("File/Folder does not exist " + collection);
        }
        // Perform a query?
        HashSet<String> irodsFiles = IQuest.getFiles(connection, collection);
        for (String irodsFile : irodsFiles) {
            startDownload(commandOptions, connection, irodsFile);
        }
    }

    public static void startDownload(CommandOptions commandOptions, Connection connection, String irodsFile) {
        // Keep downloading upon failures
        boolean download = false;
        while (!download) {
            try {
                Download.downloadFile(commandOptions, connection, irodsFile);
                download = true;
            } catch (JargonException e) {
                logger.error("Download failed, retry attempt...");
            } catch (FileNotFoundException e) {
                logger.error("File not found on irods " + irodsFile);
                download = true;
            }
        }
    }

    public static void downloadFile(CommandOptions commandOptions, Connection connection, String irodsFilePath) throws JargonException, FileNotFoundException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(irodsFilePath);

//        org.apache.log4j.Logger.getLogger("org.irods.jargon.core.transfer").setLevel(Level.OFF);
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Set file to local directory
        File localFile = new File(commandOptions.localDirectory +"/"+ new File(irodsFilePath).getName());
        if (commandOptions.preserve) {
            // Add path of folder to localDirectory,
            String newPath = commandOptions.localDirectory.getAbsolutePath() + "/" + irodsFile.getAbsolutePath();
            newPath = newPath.replaceAll("/+", "/");
            localFile = new File(newPath);
        }

        // If the file or folder exists
        if (localFile.exists()) {
            // If the location points to a file
            if (localFile.isFile()) {
                // Perform a simple file size check by default
                if (localFile.length() != irodsFile.length()){
                    while (localFile.exists()) {
                        logger.error("File size does not match, scheduled for downloading");
                        localFile.delete();
                    }
                } else  if (commandOptions.force) {
                    // Get local HASH
                    LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                    SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                    ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                    // Get remote hash
                    DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
                    ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
                    if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                        logger.debug("Same checksum, not going to overwrite");
                        return;
                    } else {
                        logger.info("Remove local file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                        logger.info("Does not match checksum of " + irodsFile.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                        localFile.delete();
                    }
                } else {
                    logger.debug("File already exists use --force to perform a checksum test on the file: " + localFile);
                }
            }
        }

        // THE ACTUAL TRANSFER

        // Disables the logger for the transfer as it easily gives thousands of lines... and perform the transfer
        org.apache.log4j.Logger.getLogger("org.irods.jargon.core.transfer").setLevel(org.apache.log4j.Level.INFO);
        if (!localFile.exists()) {
            if (!localFile.getParentFile().exists()) {
                logger.info("Creating directory " + localFile.getParentFile());
                localFile.getParentFile().mkdirs();
            }
            logger.info("Downloading file from " + irodsFile + " to " + localFile.getAbsolutePath());
            try {
                StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
                TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
                TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                transferOptions.setIntraFileStatusCallbacks(true);
                transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
                transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
                logger.debug("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
                logger.debug("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());
                defaultTransferControlBlock.setTransferOptions(transferOptions);
                dataTransferOperationsAO.getOperation(irodsFile, localFile, statusCallbackListener, defaultTransferControlBlock);
                logger.debug("Transfer completed");
            } catch (JargonException e) {
                if (localFile.exists()) {
                    localFile.delete();
                }
                throw new JargonException("Download failed, removed local file: " + localFile);
            }

            // Size limit to 50gb for checksum
            if (localFile.length() / (1024 * 1024 * 1024) > 50) {
                if (localFile.length() != irodsFile.length()) {
                    // Delete local file
                    while (localFile.exists()) {
                        localFile.delete();
                    }
                    throw new JargonException("Byte size incorrect, download failed");
                } else {
                    logger.info("File is too large for checksum validation but byte validation passed");
                }
            } else {
                // Checksum test!
                LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                // Get remote hash
                DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
                ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
                if (!localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                    logger.error("Does not match checksum of " + irodsFile.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                    localFile.delete();
                    throw new JargonException("Download failed! Incomplete / different checksum detected " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                }
            }
        }
    }
}
