package nl.wur.ssb;

import nl.wur.ssb.Connection;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.query.*;

import java.io.File;
import java.util.HashSet;
import java.util.List;

public class IQuest {
    public static HashSet<String> getFiles(Connection connection, File irodsFolder) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Get unprocessed files
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtain all TTL files in /Unprocessed folder


        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, irodsFolder + "%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(99999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            throw new JargonQueryException("No results found with " + irodsFolder);
        } else {
            System.err.println("Found " + irodsQueryResultSetResults.size() + " things...");
        }

        HashSet<String> filePaths = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            filePaths.add(path);
        }
        return filePaths;
    }
}
